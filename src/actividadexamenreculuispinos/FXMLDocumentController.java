/*
this.player.setPrevY(this.player.getPrevY() + 1);
 */
package actividadexamenreculuispinos;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author luis
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    GridPane gridpane;
    
    @FXML
    Button btn_baja, btn_primera,btn_segunda, btn_tercera,btn_cuarta;
    
    @FXML
    Circle circulo_baja,circulo_primera,circulo_segunda,circulo_tercera, circulo_cuarta;
    
    @FXML
    Rectangle rectangulo;
    
    @FXML
    ListView<String> lista;
    
    private ObservableList<String> Data = FXCollections.observableArrayList();
    
    private boolean primera = false;
    private boolean segunda = false;
    private boolean tercera = false;
    private boolean cuarta = false;
    private boolean baja = false;
    private int plantaActual = 0;
    
    private List<Circle> circulos;
    
@Override
    public void initialize(URL url, ResourceBundle rb) {
        if (rectangulo != null) {
            System.out.println("Rectángulo inyectado correctamente.");
            rectangulo.setOpacity(1);
        } else {
            System.out.println("¡El rectángulo no se inyectó correctamente!");
        }
        circulos = Arrays.asList(circulo_baja, circulo_primera, circulo_segunda, circulo_tercera, circulo_cuarta);
        cambiarPlanta(0); // Configuración inicial en planta baja
    }

    @FXML
    public void botonprimera() {
        cambiarPlanta(1);
    }

    @FXML
    public void botonsegunda() {
        cambiarPlanta(2);
    }

    @FXML
    public void botontercera() {
        cambiarPlanta(3);
    }

    @FXML
    public void botoncuarta() {
        cambiarPlanta(4);
    }

    @FXML
    public void botonbaja() {
        cambiarPlanta(0);
    }

    private void cambiarPlanta(int nuevaPlanta) {
        if (plantaActual == nuevaPlanta) {
            Data.add("Llamado desde la planta " + getNombrePlanta(plantaActual) + ", estamos ya aquí en la " + getNombrePlanta(plantaActual));
        } else if (plantaActual < nuevaPlanta) {
            Data.add("Llamado desde la planta " + getNombrePlanta(plantaActual) + ", estamos subiendo a la " + getNombrePlanta(nuevaPlanta));
        } else {
            Data.add("Llamado desde la planta " + getNombrePlanta(plantaActual) + ", estamos bajando a la " + getNombrePlanta(nuevaPlanta));
        }

        actualizarInterfaz(nuevaPlanta);
    }

    private void actualizarInterfaz(int nuevaPlanta) {
        circulos.forEach(circle -> circle.setOpacity(0));
        circulos.get(nuevaPlanta).setOpacity(1);
        
        int fila = 4 - nuevaPlanta;
        gridpane.getChildren().remove(rectangulo);
        gridpane.add(rectangulo, 1, fila);

        lista.setItems(Data);
        plantaActual = nuevaPlanta;
    }

    private String getNombrePlanta(int planta) {
        switch (planta) {
            case 0: return "baja";
            case 1: return "primera";
            case 2: return "segunda";
            case 3: return "tercera";
            case 4: return "cuarta";
            default: return "desconocida";
        }
    }
}